#!/usr/bin/env python
# -*- encoding: utf-8 -*-


class LookAndSay():

    def generate(self, num):
        count = 1
        new = []
        pos = 0
        num = str(num)
        l = len(num)

        # while there are letters left in the source string
        while pos < l - 1:

            # if the current letter is the same as the next
            if num[pos] == num[pos + 1]:
                count = count + 1

            # else the next letter a different letter
            else:
                new.append(str(count))
                new.append(num[pos])
                count = 1

            pos = pos + 1

        # add last counter because the loop doesn't do that
        new.append(str(count))
        new.append(num[pos])

        return "".join(new)
