# About

Implementation of the Look-and-Say Sequence.

# References

- [Wikipedia Article on the Look-and-Say Sequence](https://en.wikipedia.org/wiki/Look-and-say_sequence)
- [The Look-and-Say Sequence in the On-Line Encyclopedia of Integer Sequences](https://oeis.org/A005150)
